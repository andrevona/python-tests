from time import sleep

from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy.sql import select
from sqlalchemy.exc import InvalidRequestError
# from psycopg2 import OperationalError

engine = create_engine('postgresql://postgres:teste123!@localhost:5432/andre')
# engine = create_engine('postgresql://postgres:123Mudar!@localhost:5432/andre')
connection = engine.connect()

metadata = MetaData()

# 1) CREATE
artifact_type = Table('artifact_type', metadata,
                      Column('id', Integer, primary_key=True),
                      Column('name', String)
                      )

artifact = Table('artifact', metadata,
                 Column('id', Integer, primary_key=True),
                 Column('name', String),
                 Column('size', Integer),
                 Column('type', Integer, ForeignKey('artifact_type.id'))
                 )

# metadata.create_all(engine)

# 1.1) INSERT
# data = [
#     {'id': 1, 'name': 'Malware'},
#     {'id': 2, 'name': 'Trusted'},
#     {'id': 3, 'name': 'Undefined'},
# ]
# connection.execute(artifact_type.insert(), data)
# data = [
#     {'id': 1, 'name': 'teste01', 'size': 20, 'type': 1},
#     {'id': 2, 'name': 'teste02', 'size': 20, 'type': 1},
#     {'id': 3, 'name': 'teste03', 'size': 20, 'type': 2},
#     {'id': 4, 'name': 'teste04', 'size': 20, 'type': 2},
#     {'id': 5, 'name': 'teste05', 'size': 20, 'type': 3},
#     {'id': 6, 'name': 'teste06', 'size': 20, 'type': 3},
#     {'id': 7, 'name': 'teste07', 'size': 20, 'type': 3},
# ]
# connection.execute(artifact.insert(), data)
#
# ins = artifact.insert()
# connection.execute(ins, id=8, name='teste08', size=100, type=1)
#
# ins = artifact_type.insert()
# connection.execute(ins, id=4, name='teste')


# 2) UPDATE
# update = artifact_type.update().where(artifact_type.c.id == 4).values(name='testing')
# connection.execute(update)
#
#
# # 3) DELETE
# delete = artifact_type.delete().where(artifact_type.c.name.startswith('te%'))
# connection.execute(delete)


# 4) READ
def read():
    count = 1
    transaction = connection.begin()
    while True:
        sleep(3)
        print(count)
        count = count + 1
        try:
            s = select([artifact_type.c.id, artifact_type.c.name])
            result = connection.execute(s)

            print('artifact_type table:')
            for row in result:
                print('id:', row[artifact_type.c.id], ';',
                      'name:', row[artifact_type.c.name])

            result.close()
        except Exception as err:
            print(f'Error - {err}')
            # transaction.close()
            transaction.rollback()


if __name__ == '__main__':
    read()
