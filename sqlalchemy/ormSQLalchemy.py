from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

engine = create_engine('postgresql://postgres:teste123.@localhost:5432/postgres', echo=False)
Base = declarative_base()

Session = sessionmaker(bind=engine)
session = Session()


class Category(Base):
    __tablename__ = 'categories'
    __table_args__ = ({"schema": "public"})

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

    def __repr__(self):
        return "<Category(name='%s')>" % self.name

    @staticmethod
    def insert_categories():
        try:
            data = [
                Category(name='Malware'),
                Category(name='Trusted'),
                Category(name='Undefined')
            ]
            session.add_all(data)
            session.commit()
            print('\nCategories added')

        except exc.IntegrityError:
            session.rollback()
            print("\nYou can't add the same categories more than once")


class Artifact(Base):
    __tablename__ = 'artifacts'
    __table_args__ = ({"schema": "public"})

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    size = Column(Integer, nullable=False)
    type_id = Column(Integer, ForeignKey('public.categories.id'), nullable=False)
    category = relationship('Category', backref="artifacts")

    def __repr__(self):
        return "<Artifact(name='%s')>" % self.name

    @staticmethod
    def insert_artifacts():
        name = input("Artifact name: ")
        while name in (x.name for x in session.query(Artifact.name).all()):
            session.rollback()
            print("\nName already exists")
            name = input("\nArtifact name: ")

        size = int(input("Artifact size: "))

        type_id = int(input("Artifact type id: "))

        if type_id not in (x.id for x in session.query(Category.id).all()):
            session.rollback()
            raise ValueError("Type id provided is not from a valid artifact category")

        data = [Artifact(name=name, size=size, type_id=type_id)]

        session.add_all(data)
        session.commit()
        print('\nArtifact added')

    @staticmethod
    def find_artifact():
        _id = int(input("Artifact ID: "))

        if _id not in (x.id for x in session.query(Artifact.id).all()):
            session.rollback()
            raise ValueError("Artifact does not exist")

        artifact = session.query(Artifact).filter_by(id=_id).first()
        return artifact

    @staticmethod
    def update_artifact_name():
        artifact = Artifact.find_artifact()

        try:
            new_name = input('Type new artifact name: ')
            artifact.name = new_name

            session.commit()
            print("\nArtifact updated!")
        except exc.IntegrityError:
            session.rollback()
            print('Name already exists')

    @staticmethod
    def delete_from_artifact_table():
        artifact = Artifact.find_artifact()

        session.delete(artifact)
        session.commit()
        print("\nArtifact deleted!")

    @staticmethod
    def artifact_lister():
        selection = session.query(Artifact.id,
                                  Artifact.name,
                                  Artifact.size,
                                  Artifact.type_id,
                                  Category.name
                                  ).filter(Artifact.type_id == Category.id).order_by(Artifact.id)

        print('\n')
        for id, name, size, type_id, classification in selection:
            print(f'id = {id}; name = {name}; size = {size}; type_id = {type_id} ({classification})')

    @staticmethod
    def find_by_artifact_type():
        _id = int(input("Category ID: "))

        if _id not in (x.id for x in session.query(Category.id).all()):
            raise ValueError("Category does not exist")

        selection = session.query(Artifact.id,
                                  Artifact.name,
                                  Artifact.size
                                  ).filter_by(type_id=_id)

        print('\n')
        for id, name, size in selection:
            print(f'id = {id}; name = {name}; size={size}')


Base.metadata.create_all(engine)


def get_user_choice():
    print("\n[1] Insert the three basic categories (use it only once).")
    print("[2] Insert artifact.")
    print('[3] List existing artifacts')
    print('[4] List all artifacts from a given category')
    print('[5] Update artifact name')
    print('[6] Delete artifact')
    print("[q] Quit.")
    return input('Choose an option from above: ')


if __name__ == '__main__':
    choice = ''
    while choice != 'q':
        choice = get_user_choice()
        if choice == '1':
            Category.insert_categories()
        elif choice == '2':
            Artifact.insert_artifacts()
        elif choice == '3':
            Artifact.artifact_lister()
        elif choice == '4':
            Artifact.find_by_artifact_type()
        elif choice == '5':
            Artifact.update_artifact_name()
        elif choice == '6':
            Artifact.delete_from_artifact_table()
        elif choice == 'q':
            print('\nQuiting now')
        else:
            print('\nPlease choose a valid option')
