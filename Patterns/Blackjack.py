from random import shuffle

class Baralho:
    def __init__(self):
        self.valores = "2 3 4 5 6 7 8 9 10 Valete Dama Rei".split(' ')
        self.naipes = ['Paus', 'Copas', 'Espada', 'Ouro']
        
        self.cartas = list((valor, naipe) for valor in self.valores for naipe in self.naipes)
        shuffle(self.cartas)

    def pega_carta(self):
        return self.cartas.pop()


def comecar():
    deck = Baralho()

    def pedir():
        return deck.pega_carta()
    
    def verificar_valor(valor):
        if (valor == "Valete" or valor == "Dama" or valor == "Rei"):
            valor = 10
        return int(valor)

    soma = 0
    def checar_status(soma):
        print("-------------------------")
        if(soma > 21):
            print(f'Soma final => {soma}. Estourou :(')
        else:
            print(f'Soma parcial => {soma}')

    print("O jogo começou")
    print("-------------------------")

    x = 's'
    while(x == 's' and soma <= 21):
        x = input("Quer uma carta? [s/n]: ")
        if(x == 's'):
            carta = pedir()
            valor = carta[0]
            naipe = carta[1]

            valor = verificar_valor(valor)

            soma += valor
            print(f'Carta recebida => {carta[0]} de {naipe}')

            checar_status(soma)
        else:
            print("-------------------------")
            print(f'Soma final => {soma}.')

    print("o jogo acabou")

comecar()