class Car(object):
    @staticmethod
    def factory(type):
        if type == "RaceCar":
            return RaceCar()
        if type == "Van":
            return Van()
        assert 0, "Bad car request: " + type


class RaceCar(Car):
    def drive(self):
        print("RaceCar driving.")


class Van(Car):
    def drive(self):
        print("Van driving.")


obj = Car.factory("Van")
obj.drive()
