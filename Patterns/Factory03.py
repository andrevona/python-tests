BaseClass = type("BaseClass", (object,), {})


@classmethod  # chamar funções dentro de classes sem precisar instanciá-las
def Check1(self, myStr):
    return myStr == "ham"

@classmethod
def Check2(self, myStr):
    return myStr == "sandwich"


subclasse1 = type("subclasse1", (BaseClass,), {"x": 1, "Check": Check1})
subclasse2 = type("subclasse2", (BaseClass,), {"x": 100, "Check": Check2})
# subclasse3 = type("subclasse3", (BaseClass,), {"x": 100, "Check": Check2})
# subclasse4 = type("subclasse4", (BaseClass,), {"x": 100, "Check": Check2})
# subclasse5 = type("subclasse5", (BaseClass,), {"x": 100, "Check": Check2})
# subclasse6 = type("subclasse6", (BaseClass,), {"x": 100, "Check": Check2})


def myFactory(myStr):
    for subcls in BaseClass.__subclasses__():
        print(f'myStr = {myStr} /// subcls = {subcls} /// subcls.Check(myStr) = {subcls.Check(myStr)}')
        if subcls.Check(myStr):
            return subcls()


print('Criando obj1')
obj1 = myFactory("ham")
print('obj1 criado\n')

print('Criando obj2')
obj2 = myFactory("sandwich")
print('obj2 criado\n')

print(obj1.x)
print(obj2.x)

# obj3 = myFactory("asdasd")
# print(obj3.x) #--> gera erro 'NoneType', pois não tem objeto com o atributo 'x'
