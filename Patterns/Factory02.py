# ParentClass = type("ParentClass", (object,), {})
class ParentClass:
    pass


c1 = type("c1", (ParentClass,), {"y": 20})
c2 = type("c2", (ParentClass,), {"y": 2000})


def my_fac(my_bool):
    return c1() if my_bool else c2()


test1 = my_fac(True)
test2 = my_fac(False)

for sub in ParentClass.__subclasses__():
    print(sub)

print(test1.y)
print(test2.y)
