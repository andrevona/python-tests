# 1)
# class OnlyOne:
#     class __OnlyOne:
#         def __init__(self, arg):
#             self.val = arg
#
#         def __str__(self):
#             return repr(self) + self.val
#
#     instance = None
#
#     def __init__(self, arg):
#         if not OnlyOne.instance:
#             OnlyOne.instance = OnlyOne.__OnlyOne(arg)
#         else:
#             OnlyOne.instance.val = arg
#
#     def __getattr__(self, name):
#         return getattr(self.instance, name)
#
#
# x = OnlyOne('sausage')
# print(x)
#
# y = OnlyOne('eggs')
# print(y)
#
# z = OnlyOne('spam')
# print(z)
# print(x)
# print(y)


# 2)
# class Borg:
#     _shared_state = {}
#
#     def __init__(self):
#         self.__dict__ = self._shared_state
#
#
# class Singleton(Borg):
#     def __init__(self, arg):
#         Borg.__init__(self)
#         self.val = arg
#
#     def __str__(self): return self.val
#
#
# x = Singleton('sausage')
# print(x)
#
# y = Singleton('eggs')
# print(y)
#
# z = Singleton('spam')
# print(z)
# print(x)
# print(y)


# 3)
# class Singleton1(object):
#     def __new__(cls):
#         if not hasattr(cls, 'instance') or not cls.instance:
#             cls.instance = super().__new__(cls)
#         return cls.instance
#
#
# obj1 = Singleton1()
# obj2 = Singleton1()
#
# print(obj1 is obj2)
# print(obj1 == obj2)
# print(type(obj1) == type(obj2))
# print(id(obj1) == id(obj2))


# 4)
# def singleton(my_class):
#     instances = {}
#
#     def get_instance(*args, **kwargs):
#         if my_class not in instances:
#             instances[my_class] = my_class(*args, **kwargs)
#         return instances[my_class]
#     return get_instance
#
#
# @singleton
# class Test(object):
#     pass
#
#
# obj1 = Test()
# obj2 = Test()
#
# print(obj1 is obj2)
# print(obj1 == obj2)
# print(type(obj1) == type(obj2))
# print(id(obj1) == id(obj2))


# 5) Singleton + Metaclasse
class Singleton(type):
    instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.instances:
            print(f'1) cls = {cls}')
            print(f'2) cls.instances = {cls.instances}\n')

            cls.instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)

            print(f'3) cls.instances = {cls.instances}')
            print(f'4) cls.instances[cls] = {cls.instances[cls]}\n')
        else:
            print(f'Instanciar um segundo objeto faz cair no else e retornar o mesmo objeto\n')
        return cls.instances[cls]


class MyClass(metaclass=Singleton):
    """
    se tirar o parâmetro, vão ser criadas instâncias diferentes
    """


print('\ncriando a instância "a"...\n')
a = MyClass()

print('INSTÂNCIA "a" FOI CRIADA!!\n')

print('criando a instância "b"...\n')
b = MyClass()

print(f'a é b? {a is b}')
