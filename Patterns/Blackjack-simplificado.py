from random import shuffle

class Baralho:
    def __init__(self):
        self.valores = "As 2 Rei".split(' ') #As 2 3 4 5 6 7 8 9 10 Valete Dama Rei
        self.naipes = ['Paus', 'Copas', 'Espada', 'Ouro']
        
        self.cartas = list((valor, naipe) for valor in self.valores for naipe in self.naipes)
        shuffle(self.cartas)

    def pega_carta(self):
        return self.cartas.pop()


def comecar():
    deck = Baralho()

    def pedir():
        return deck.pega_carta()
    
    def verificar_valor(valor):
        if (valor == "Valete" or valor == "Dama" or valor == "Rei"):
            valor = 10
        elif (valor == "As"):
            valor = input(f'O As de vale 1 ou 11? ')
        return int(valor)

    soma = 0

    print("O jogo começou")
    
    x = 's'
    while(x == 's'):
        print("-------------------------")
        x = input("Quer uma carta? [s/n]: ")
        if(x == 's'):
            carta = pedir()
            valor = carta[0]
            naipe = carta[1]

            print(f'Carta recebida => {valor} de {naipe}')            
            valor = verificar_valor(valor)

            soma += valor
            print("-------------------------")
            print(f'Soma parcial => {soma}')
        else:
            print("-------------------------")
            print(f'Soma final => {soma}.')

    print("O jogo acabou")

comecar()