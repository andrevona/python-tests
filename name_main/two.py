from name_main import one

print("\ntop-level no arquivo two.py")
one.func()

print(f'\nnome de one.py => {one.__name__}')
print(f'nome de two.py => {__name__}')

if __name__ == "__main__":
    print("two.py rodado diretamente")
else:
    print("two.py foi importado por outro módulo")
