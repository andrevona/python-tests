def func():
    print("função func() no arquivo one.py foi chamada")


print("top-level no arquivo one.py")

if __name__ == "__main__":
    print("one.py rodado diretamente")
else:
    print("one.py foi importado por outro módulo")
