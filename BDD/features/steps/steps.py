# 1h 42 min
from behave import step

from BDD.features.steps.calc import somar


@step('somar "{val_1}" com "{val_2}"')
def test_soma(context, val_1, val_2):
    context.result = float(somar(float(val_1), float(val_2)))


@step('o resultado deve ser "{resultado}"')
def test_resultado(context, resultado):
    assert context.result == float(resultado)
