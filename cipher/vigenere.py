"""Alfabeto."""
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def main():
    u"""
    Função principal.

    Teste.
    """
    my_message = input("Informe a mensagem: ")
    my_key = input("Informe a chave: ")
    my_mode = 'encrypt'  # set to 'encrypt' or 'decrypt'

    print('\nPlain text:')
    print(my_message)
    print()
    print('Key to encrypt:')
    print(my_key)
    print()

    translated = encrypt_message(my_key, my_message)

    if my_mode == 'decrypt':
        translated = decrypt_message(my_key, my_message)

    print('%sed message:' % (my_mode.title()))
    print(translated)


def encrypt_message(key, message):
    u"""Função que encripta a mensagem passada."""
    return translateMessage(key, message, 'encrypt')


def decrypt_message(key, message):
    u"""Função que decripta a mensagem passada."""
    return translateMessage(key, message, 'decrypt')


def translateMessage(key, message, mode):
    u"""Função que encripta a mensagem passada."""
    translated = []  # stores the encrypted/decrypted message string

    key_index = 0
    key = key.upper()

    for symbol in message:  # loop through each character in message
        num = LETTERS.find(symbol.upper())
        if num != -1:  # -1 means symbol.upper() was not found in LETTERS
            if mode == 'encrypt':
                num += LETTERS.find(key[key_index])  # add if encrypting
            elif mode == 'decrypt':
                num -= LETTERS.find(key[key_index])  # subtract if decrypting

            num %= len(LETTERS)  # handle the potential wrap-around

            # add the encrypted/decrypted symbol to the end of translated.
            if symbol.isupper():
                translated.append(LETTERS[num])
            elif symbol.islower():
                translated.append(LETTERS[num].lower())

            key_index += 1  # move to the next letter in the key
            if key_index == len(key):
                key_index = 0
        else:
            # The symbol was not in LETTERS, so add it to translated as is.
            translated.append(symbol)
    return ''.join(translated)


if __name__ == '__main__':
    main()
