class MyKlass(object):
    foo = 2


x = MyKlass()

# MyKlass é a classe a partir da qual o obj 'x' foi instanciado
print(x.__class__)

# 'type' é a (meta)classe a partir da qual MyKlass foi instanciada
print(MyKlass.__class__)
