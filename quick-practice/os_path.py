import os


actual_path = os.path.dirname(__file__)
print(f'\nactual path => {actual_path}')

file_on_actual_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'TEST.txt'))
print(f'\nfile_on_actual_path => {file_on_actual_path}')

file_on_previous_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..//../..', 'TEST.txt'))
print(f'\n\nfile_on_previous_path => {file_on_previous_path}')

file_on_previous_path2 = os.path.join(os.path.dirname(__file__), '..//../..', 'TEST.txt')
print(f'\nfile_on_previous_path2 => {file_on_previous_path2}\n(no ABS path here)')
