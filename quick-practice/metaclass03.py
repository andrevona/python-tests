# __call__ chamado com a classe já criada p/ instanciar objeto
class MyMetaClass(type):
    def __call__(cls, *args, **kwds):
        print('__call__ da Classe', str(cls))
        print('__call__ *args =', str(args))
        return type.__call__(cls, *args, **kwds)


class MyKlass(object, metaclass=MyMetaClass):
    def __init__(self, a, b):
        print(f'MyKlass object with a={a}, b={b}')


class Teste1(MyKlass):
    pass


class Teste2(MyKlass):
    pass


print('--vamos criar o objeto "foo", instância de MyKlass--')
foo = MyKlass(1, 2)

print('\n--vamos criar o objeto "bar", instância de Teste1 (subclasse de MyKlass)--')
bar = Teste1(10, 20)

print(f'\n\nQuais são as subclasses de MyKlass? {MyKlass.__subclasses__()}')
print(f'\nQual a metaclasse de MyKlass? {MyKlass.__class__}')
print(f'Qual a metaclasse de Teste1? {Teste1.__class__}')