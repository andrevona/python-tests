def decorador(f):
    def wrap():
        return "foda-se"
    return wrap


@decorador
def falar():
    return "Olar mundo!!"


def decorador2(f):
    def wrap(*args, **kwargs):
        return f(*args, **kwargs)
    return wrap

@decorador2
def soma(a, b):
    return a + b
