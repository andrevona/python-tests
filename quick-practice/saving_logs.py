import os
from datetime import datetime
import time

origin_path = '/home/andrevona/Documents/lots_of_files/'
destiny_path = '/home/andrevona/Documents/single_file/'


def create_files(qty):
    for x in range(qty):
        with open(origin_path + "file" + str(x), 'w') as f:
            f.write("file " + str(x) + " - line1\n" +
                    "file " + str(x) + " - line2\n" +
                    "file " + str(x) + " - line3\n"
                    )


create_files(5)

while True:
    try:
        file_list = os.listdir(origin_path)
        if file_list:
            for file in file_list:
                file_path = os.path.join(origin_path, file)
                lines = open(file_path, 'r').readlines()
                for line in lines:
                    hora = datetime.now()
                    msg = '[{}] '.format(datetime.isoformat(hora))
                    open(destiny_path + "all_lines.txt", 'a').write(msg + line)
                os.remove(file_path)
                time.sleep(1)

        else:
            print('No more files!')
            break

    except Exception as e:
        print("error - " + str(e))
        break
