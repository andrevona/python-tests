# import numpy as np
# import scipy.stats as st
import statsmodels.stats.api as sms


DEZ = [23126, 26013, 30118, 31417, 30759, 35685, 28226, 33460, 32271, 38992, 45176, 42768, 50452, 40347, 16806,
       3551, 14136, 21729, 16301, 19373, 40267, 42631, 36216, 53143, 49143, 49696, 20335, 27327, 42533, 26424, 25879]

NOV = [41472, 51045, 45729, 47226, 43236, 6737, 20672, 22923, 10554, 15344, 13661, 17332, 6811, 17856, 36722,
       36270, 35573, 31611, 17524, 6027, 20460, 24660, 18003, 25511, 23526, 30740, 2826, 4096, 6131, 32777]

OUT = [19321, 20340, 25874, 11248, 11155, 15499, 18985, 28782, 37623, 40442, 28141, 35410, 39251, 28692, 26864, 31124,
       33464, 26439, 28757, 25544, 26235, 57692, 10193, 13478, 528, 10001, 16181, 47270, 57837, 13479, 11116]

SET = [15378, 15741, 19763, 17144, 19454, 30140, 22868, 2587, 9911, 24564, 27694, 30552, 37497, 37571, 33535,
       30527, 34003, 20047, 28222, 25155, 21234, 17915, 14114, 15816, 17887, 18336, 16682, 16724, 27947, 29506]


# result_dez_1 = st.t.interval(0.99, len(DEZ)-1, loc=np.mean(DEZ), scale=st.sem(DEZ))
# result_tot_1 = st.t.interval(0.99, len(DEZ + NOV + OUT + SET)-1,
#                              loc=np.mean(DEZ + NOV + OUT + SET), scale=st.sem(DEZ + NOV + OUT + SET))

# print(f'Dezembro: {result_dez_1}')
# print(f'Novembro: {result_nov_1}')
# print(f'TOTAL --> {result_tot_1}')

interval = 0.05

result_dez_2 = sms.DescrStatsW(DEZ).tconfint_mean(alpha=interval)
result_nov_2 = sms.DescrStatsW(NOV).tconfint_mean(alpha=interval)
result_out_2 = sms.DescrStatsW(OUT).tconfint_mean(alpha=interval)
result_set_2 = sms.DescrStatsW(SET).tconfint_mean(alpha=interval)
result_TOT_2 = sms.DescrStatsW(DEZ + NOV + OUT + SET).tconfint_mean(alpha=interval)

print(f'Intervalo de confiança = {(1 - interval) * 100}%\n')
print(f'Dezembro: {result_dez_2}')
print(f'Novembro: {result_nov_2}')
print(f'Outubro : {result_out_2}')
print(f'Setembro: {result_set_2}')
print(f'TOTAL --> {result_TOT_2}')

# excel: 30000,35000,40000,45000
