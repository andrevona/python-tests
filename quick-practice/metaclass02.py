# __init__ e __new__ chamados apenas no momento da criação da classe
class MinhaMetaClasse(type):
    def __new__(meta, name, bases, dct):
        print('-----------------------------------')
        print("Alocando memória para a classe", name)
        print(meta)
        print(bases)
        print(dct)
        return super(MinhaMetaClasse, meta).__new__(meta, name, bases, dct)

    def __init__(cls, name, bases, dct):
        print('-----------------------------------')
        print("Inicializando a classe", name)
        print(cls)
        print(bases)
        print(dct)
        super(MinhaMetaClasse, cls).__init__(name, bases, dct)


class MinhaClasse(object, metaclass=MinhaMetaClasse):
    def foo(self, param):
        pass
    barattr = 2
