class A:
    def __init__(self):
        self.atributo1 = 10


class B:
    def __init__(self):
        self.atributo1 = 9

    def metodo1(self):
        print('metodo do B')


class C:
    def __init__(self):
        self.atributo1 = 8

    def metodo1(self):
        print('metodo do C')

    @property
    def prop1(self):
        print('propriedade do C')
        return "retorno da propriedade do C"


a1 = A()
b1 = B()
c1 = C()


if __name__ == '__main__':
    print(a1.atributo1)
    b1.metodo1()
    c1.prop1
