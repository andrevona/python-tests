class Char(object):
    def __init__(self, name):
        self.health = 100
        self.name = name

    def print_name(self):
        print (self.name)


class Blacksmith(Char):
    def __init__(self, name, forge_name):
        super(Blacksmith, self).__init__(name)
        self.forge = Forge(forge_name)


class Forge:
    def __init__(self, forge_name):
        self.name = forge_name


bs = Blacksmith('Bob', 'Rick Forge')
bs.print_name()
print(bs.forge.name)
print(Char.__subclasses__())
