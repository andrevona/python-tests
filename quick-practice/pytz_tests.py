import datetime
import pytz

a = datetime.datetime(2018, 1, 1, 3, tzinfo=pytz.timezone('America/Sao_Paulo'))
b = datetime.datetime(2018, 1, 1, 6, tzinfo=pytz.timezone('UTC'))

print(f'\nSP ==> {a}')
print(f'UTC => {b}\n')
diff = (a - b)
# print(dir(diff))
print(f'Difference between Sao Paulo and UTC => {diff}')
print(f'Difference between Sao Paulo and UTC => {diff.seconds} seconds\n\n----------')


x = datetime.datetime(2018, 1, 1, 10)
y = x - datetime.timedelta(minutes=10)


# time now:
hora = datetime.datetime.now()

# tz info:
tz_sp = pytz.timezone('America/Sao_Paulo')
tz_utc = pytz.timezone('UTC')

# with timezone:
hora_sp = tz_sp.localize(hora)
hora_utc = hora_sp.astimezone(tz_utc)
# hora_utc = tz_utc.localize(hora)

print(f'\nHorário regular {hora}')
print(f'Timezone SP ==> {hora_sp}')
print(f'Timezone UTC => {hora_utc}')
print(f'TZ SP = TZ UTC? {hora_sp == hora_utc}')
