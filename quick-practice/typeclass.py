def printarAlgo(self, name):
    self.name = name
    print(name)


Test = type("Test", (object,), {"attribute": 5, "method": printarAlgo})

t = Test()
t.method('Método "printarAlgo"')

print(t.attribute)
