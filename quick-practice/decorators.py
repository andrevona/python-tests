def add_one(my_func):
    def add_one_inside(*args, **kwargs):
        return my_func(*args, **kwargs) + 1
    return add_one_inside()


@add_one
def old_func(x=213):
    return x
