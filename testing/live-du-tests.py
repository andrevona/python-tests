from unittest import TestCase, main

from testing.functions import eh_par, soma, sub

# 60 min
# https://youtu.be/5hL9T3jintE


class Testes1(TestCase):
    def test_sum(self):
        self.assertEqual(soma(4, 5), 9)

    def test_sub(self):
        self.assertEqual(sub(5, 4), 1)

    # def test_soma_errada(self):
    #     self.assertEqual(soma(3, 4), 4)


class Testes2(TestCase):
    def test_eh_par(self):
        self.assertTrue(eh_par(4))

    def test_eh_impar(self):
        self.assertFalse(eh_par(5))

    def test_eh_float(self):
        self.assertFalse(eh_par(3.6))


if __name__ == '__main__':
    main()
