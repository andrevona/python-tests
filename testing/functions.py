def soma(x, y):
    return x + y


def sub(x, y):
    return x - y


def eh_par(val):
    if isinstance(val, int) or isinstance(val, float):
        return True if val % 2 == 0 else False
    else:
        return False
