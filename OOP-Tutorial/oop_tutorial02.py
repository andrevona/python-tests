# link => https://www.youtube.com/watch?v=BJ-VvGyQxho&index=2&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc

# Class Variables => the same for each instance (shared among all instances of a Class)
# Instance Variables => can be unique for each instance (name, pay)


class Employee:
    raise_amount = 1.05  # class variable
    num_of_emp = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first[0] + "." + last.replace(" ", "") + "@company.com"
        Employee.num_of_emp += 1  # does not allow overriding the value (instead of self.num)

    def fullname(self):
        return f'{self.first} {self.last}'

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)  # allows overriding the amount


print(f"Number of employees: {Employee.num_of_emp}")
emp_1 = Employee("Andre", "Von Ah", 1500)
emp_2 = Employee("Test", "User", 4200)

print(emp_1.pay)
emp_1.apply_raise()
print(emp_1.pay)
print(f"Number of employees: {Employee.num_of_emp} \n-------------")

# o atributo é procurado primeiro na instância, depois na Classe (ou nas Classes da qual ela é criada)
print(Employee.raise_amount)
print(emp_1.raise_amount)
print(str(emp_2.raise_amount) + "\n-------------")

print(emp_1.__dict__)
emp_1.raise_amount = 1.25
print(emp_1.__dict__)
print("raise_amount agora é atributo de emp_1\n-------------")

print(Employee.raise_amount)
print(emp_1.raise_amount)
print(emp_2.raise_amount)


