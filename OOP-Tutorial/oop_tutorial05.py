# link => https://www.youtube.com/watch?v=3ohzBxoFHAY&index=5&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc


class Employee:
    raise_amount = 1.05

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first[0] + "." + last.replace(" ", "") + "@company.com"

    def fullname(self):
        return f'{self.first} {self.last}'

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)  # allows overriding the amount

    # def __repr__(self):
    #     pass
    #
    # def __str__(self):
    #     pass


emp1 = Employee("Andre", "Von Ah", 10000)
emp2 = Employee("Test", "User", 12000)
