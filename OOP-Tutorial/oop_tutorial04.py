# link => https://www.youtube.com/watch?v=RSl87lqOXDE&index=4&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc


class Employee:
    raise_amt = 1.05  # class variable
    num_of_emp = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first[0] + "." + last.replace(" ", "") + "@company.com"
        Employee.num_of_emp += 1  # does not allow overriding the value (instead of self.num)

    def fullname(self):
        return f'{self.first} {self.last}'

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amt)  # allows overriding the amount


class Developer(Employee):
    raise_amt = 1.10

    def __init__(self, first, last, pay, pro_lang):
        # Employee.__init__(self, first, last, pay)
        super().__init__(first, last, pay)
        self.pro_lang = pro_lang


class Manager(Employee):
    def __init__(self, first, last, pay, employees=None):
        super().__init__(first, last, pay)
        if employees is None:
            self.employees = []
        else:
            self.employees = employees

    def add_emp(self, emp):
        if emp not in self.employees:
            self.employees.append(emp)

    def remove_emp(self, emp):
        if emp in self.employees:
            self.employees.remove(emp)

    def print_emp(self):
        for emp in self.employees:
            print('=>', emp.fullname())


print(f"Number of employees: {Employee.num_of_emp}")
dev_1 = Developer("Andre", "Von Ah", 1000, 'Python')
dev_2 = Developer("Test", "User", 2000, 'Java')

# print(f'email = {dev_1.email}, language = {dev_1.pro_lang}')
# print(dev_2.email)

print(f"Number of employees: {Employee.num_of_emp}")
mgr1 = Manager('Sue', 'Smith', 9000, [dev_1])
print(mgr1.email)
mgr1.print_emp()

mgr1.add_emp(dev_2)
mgr1.remove_emp(dev_1)
mgr1.print_emp()

print(f"Number of employees: {Employee.num_of_emp}")

print(isinstance(mgr1, Employee))
print(issubclass(Manager, Developer))
# print(help(Developer))
