# link => https://www.youtube.com/watch?v=ZDa-Z5JzLYM&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc&index=1


class Employee:
    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first[0] + "." + last.replace(" ", "") + "@company.com"

    def fullname(self):
        return f'{self.first} {self.last}'


emp_1 = Employee("Andre", "Von Ah", 1500)

print(emp_1.fullname())
print(Employee.fullname(emp_1))
print(emp_1.email)
