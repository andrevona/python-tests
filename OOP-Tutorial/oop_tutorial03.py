import datetime
# link => https://www.youtube.com/watch?v=rq8cL2XMM5M&index=3&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc

# regular method => automatically passes THE INSTANCE (self) as the first argument
# class method => automatically passes THE CLASS (cls) as the first argument (alternative constructor)
# static method => doesn't pass anything and doesn't access Instance or Class in the method


class Employee:
    raise_amount = 1.05  # class variable
    num_of_emp = 0

    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first[0] + "." + last.replace(" ", "") + "@company.com"
        Employee.num_of_emp += 1  # does not allow overriding the value (instead of self.num)

    def fullname(self):
        return f'{self.first} {self.last}'

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)  # allows overriding the amount

    @classmethod
    def set_raise_amount(cls, amount):
        cls.raise_amount = amount  # updating raise_amount Class Variable

    @classmethod
    def from_string(cls, emp_str):
        first, last, pay = emp_str.split('-')
        return cls(first, last, pay)

    @staticmethod
    def is_weekday(day):
        return False if day.weekday() in (5, 6) else True


emp_1 = Employee("Andre", "Von Ah", 1000)
emp_2 = Employee("Test", "User", 2000)

Employee.set_raise_amount(1.25)  # or
# Employee.raise_amount = 1.25
# emp_1.set_raise_amount(1.25), mas não faz sentido alterar através da instância
print(f"{Employee.raise_amount} // {emp_1.raise_amount} // {emp_2.raise_amount}")

emp_str_1 = 'Joe-Montana-30000'
emp_str_2 = 'Steve-Young-20000'
new_emp_1 = Employee.from_string(emp_str_1)  # criando novas instâncias a partir do classmethod
new_emp_2 = Employee.from_string(emp_str_2)

print(f"{new_emp_1.fullname()}, {new_emp_1.email}, {new_emp_1.pay}")


my_date = datetime.date(2018, 7, 30)
print(f'É dia da semana? {Employee.is_weekday(my_date)}')
